Nouveau Project
===============

Please file issues [here](https://gitlab.freedesktop.org/drm/nouveau/-/issues).

For now, please use the [Mailing list](https://lists.freedesktop.org/mailman/listinfo/nouveau) to submit patches.

This branch exists only to hold the CI configuration and issue templates.
